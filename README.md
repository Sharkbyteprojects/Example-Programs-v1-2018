# Easy Example Programs 2018

---
The software is free of charge under the [MIT-license](https://gitlab.com/Sharkbyteprojects/Example-Programs-v1-2018/blob/master/LICENSE) and is intended to inspire you to new software.

---
Mirror: 
- [GITLAB](https://gitlab.com/Sharkbyteprojects/Example-Programs-v1-2018)
- [GitHub](https://github.com/Sharkbyteprojects/Example-Programs-v1-2018)

---
Ideas write you [here](https://gitlab.com/Sharkbyteprojects/Example-Programs-v1-2018/issues/1).

---

This document has been translated by the Microsoft Translator.